<?php
namespace Esseasterisco\Form\Field;

class FieldCurrentPassword extends FieldPassword
{
	var $fieldtype = 'password';
	var $rules = [ 'required','string' ];
}
