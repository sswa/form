<?php
namespace Esseasterisco\Form\Field;

class FieldSubmit extends Field
{
	var $fieldtype = 'submit';
	var $template = 'submit';
	var $reset = null;
}
