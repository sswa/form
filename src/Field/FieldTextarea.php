<?php
namespace Esseasterisco\Form\Field;

class FieldTextarea extends Field
{
	var $rows = 5;
	var $fieldtype = 'textarea';
	var $template = 'textarea';

}
