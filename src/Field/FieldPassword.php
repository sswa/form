<?php
namespace Esseasterisco\Form\Field;

class FieldPassword extends Field
{
	var $fieldtype = 'password';
	var $rules = [ 'string' ];
}
